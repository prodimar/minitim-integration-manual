# Funcionalidades específicas para Solvos

## Conexiones salientes
El R-FLOW1 puede establecer una conexión con un servidor y puerto específicos. Simultáneamente, se aceptan tres conexiones con tres roles diferentes:

- **Rol 0:** control (interfaz de comandos).
- **Rol 1:** recepción de lecturas de streaming.
- **Rol 2:** miscelánea. Esta es la que se utiliza para hablar con el AP en este caso.

### El comando connect
Para iniciar una conexión se utiliza el comando `connect`:

```
action=connect&server<IP del servidor>&port=<puerto>&role=<número de rol>
```

```
action=connect&server=192.168.0.130&port=2101&role=2
```

El R-FLOW1 almacenará los parámetros IP y puerto hasta que se reinicie, e intentará reconectarse si pierde la conexión.

Para desconectarse sin reiniciar, se puede configurar IP vacía y puerto -1: 
```
action=connect&server=&port=-1&role=2
```

### El comando send

Se pueden enviar datos manualmente a la conexión con el comando `send`:

```
action=send&text=<datos a enviar>
```

Este comando puede ser útil para configurar el AP cuando este ya está conectado al R-FLOW1.

Se realiza sustitución de caracteres sobre el parámetro text: las secuencias `\XX` se sustituyen por el carácter de valor hexadecimal XX. Por ejemplo, `\0D\0A` resulta en la secuencia CRLF.

## Funcionamiento del control automático y conexiones

El controlador automático es una pieza del firmware que se coloca entre la conexión TCP/1024 y el intérprete de comandos del R-FLOW1, de modo que el R-FLOW1 puede enviarse comandos a sí mismo, como si fuera un host conectado. Utiliza la conexión saliente de rol 2 para comunicarse con el AP.

Cuando el controlador automático está funcionando, la conexión de control con un host externo a través de TCP/1024 sigue operativa. Un host conectado puede enviar comandos, y podrá recibir:

- Los eventos asíncronos y respuestas del R-FLOW1 a todos los comandos que reciba, sean enviados por un host externo o por el controlador automático.
- Los comandos enviados por el R-FLOW a sí mismo o al AP.
- Todas las respuestas y eventos asíncronos enviados por el AP.

De esta forma, la salida de TCP/1024 es un log de todos los comandos y sus respuestas enviados durante el procesamiento automático.

## Secuencia de control automático

Está contenida en una máquina de estados dentro del controlador automático, y su ejecución puede habilitarse o deshabilitarse a través de un parámetro de configuración.

La secuencia es la siguiente:

1. Espera a que la fotocélula esté cortada.

2. Si la fotocélula se corta, ejecuta un `readTag`.

3. Se espera por un tag pasivo válido. Se considera tag válido aquel con el siguiente formato:

	```
	EPC con prefijo constante/TID,RSSI,antena,timestamp
	```

	Los EPCs que no estén encabezados por el prefijo constante que acordemos se descartan.

	Si hay más de un tag válido, se selecciona aquel con mejor RSSI. Si no hay ninguno, se vuelve al paso 1.

4. Una vez tenemos un tag válido:

	a.	Se extraen los tres últimos dígitos (el radio ID).

	b.	Se conmuta el multiplexor hacia el AP.

	c.	Se envía un `SCAN` del radio ID extraído.

5. Se espera por la respuesta del tag activo. Se considera válida la respuesta que empiece por `> SCAN: 0<radio ID>`, y supere el RSSI mínimo establecido.

	Si no hay respuesta, se reintenta hasta 3 veces. Si los reintentos no tienen éxito, se va al paso 8C.

6. Si hay un tag activo válido, se envía un `ACCESS` del radio ID extraído.

7. Se espera por la respuesta. Si no hay respuesta, se reintenta hasta 3 veces. Si los reintentos no tienen éxito, se va al paso 8C.

8. Se actúa sobre las salidas digitales del AP emulando la funcionalidad del AUTOACCESS:

	8A – ACCESS OK. Se envía al AP `SETIO 0002`.

	8B – ACCESS NOK. Se envía al AP `SETIO 0001`.

	8C – NO TAGS. Se envía al AP `SETIO 0003`.

9. Se esperan 2 segundos.

10. Se envía al AP `SETIO 0000`, restaurando el valor de las salidas digitales. Se conmuta el multiplexor hacia el R-FLOW1.

	A este estado se accede si hay algún error en cualquiera de los anteriores (por ejemplo, se pierde la conexión con el AP).

11. Se espera a que la fotocélula no esté cortada para volver al inicio.

## Configuración

Para que el modo automático funcione correctamente, es necesario aplicar la siguiente configuración:

- Establecer la conexión de rol 2 con el AP868 con el comando `connect`.
- Imprimir el TID, además del EPC, de cada tag pasivo leído: `action=setRRParam&name=/reader/read/plan&value=SimpleReadPlan:[Antennas=[1],Protocol=GEN2,Filter=null,Op=ReadData:[Bank=TID,WordAddress=2,Len=4],UseFastSearch=false,Weight=1000]`.
- Los tags pasivos deben haber sido grabados con el prefijo constante que acordemos (temporalmente y hasta que se decida el prefijo definitivo, el firmware usa el prefijo `ABCD`).
- El parámetro `cpRFID_READER_OPTIONS` debe tener un valor superior a 64 (las opciones por defecto + la habilitación del modo automático dan el valor 65). 

Con el siguiente comando se configura la funcionalidad de autorun para que aplique los cambios necesarios en cada reinicio.

```
action=setParam&name=cpAUTORUNstr&value=action=setParam&name=cpRFID_READER_OPTIONS&value=65|action=setRRParam&name=/reader/read/plan&value=SimpleReadPlan:[Antennas=[1],Protocol=GEN2,Filter=null,Op=ReadData:[Bank=TID,WordAddress=2,Len=4],UseFastSearch=false,Weight=1000]|action=connect&server=<IP AP>&port=2101&role=2
```

