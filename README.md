# Minitim y R-FLOW1: manual de operación e integración

Consulta la versión actualizada del manual en [nuestro repositorio](https://bitbucket.org/prodimar/minitim-integration-manual/src/master/README.md).

## Descripción del equipo

Minitim es un equipo alimentado a batería que ofrece funcionalidades de lectura y escritura de códigos RFID, y lectura de códigos de barras, para lo que contiene un lector RFID y una cámara. Estas funcionalidades son utilizables por medio de su interfaz de usuario física, o de forma remota, a través de sus interfaces Wi-Fi y Bluetooth.

R-FLOW1 es un lector de RFID de sobremesa, con posibilidad de alimentación por USB o a través de un alimentador externo. Además de Wi-Fi y Bluetooth, cuenta con Ethernet y USB, este último con varios modos de uso.

El MiniTIM y el R-FLOW1 comparten la interfaz de control por línea de comandos, así como muchas de sus funcionalidades. En este manual se indicará cuando una funcionalidad está sólo disponible para uno de los equipos.

En las páginas [Detalles MiniTIM](minitim.md) y [Detalles R-FLOW1](rflow1.md) se describe la interfaz de usuario de cada uno y sus funciones específicas.

## Modos de activación de los lectores

Una operación de un lector puede ser disparada a través de tres métodos.

 - A través de los botones, si así está configurado.
 - A través de detección por radar (sólo disponible para MiniTIM): el equipo cuenta con un radar que puede determinar si un objeto se ha colocado delante de los lectores, y puede configurarse para disparar cualquiera de los dos lectores cuando detecte un objeto.
 - De forma remota, enviando el comando adecuado a través de la conexión de control. Este modo siempre estará activo.
 - Continuo (sólo disponible para R-FLOW1): se activa continuamente.

Si se indica la misma forma de activación para varios lectores, la lectura de códigos de barras tiene preferencia sobre la lectura de etiquetas RFID, y ésta tiene preferencia sobre la escritura.

El radar tiene dos modos de activación: por nivel y por flanco.

 - Si está configurado para activarse por flanco, disparará el lector cuando se coloque un objeto ante él, pero será necesario retirar el objeto y volverlo a colocar para generar una nueva lectura.
 - Si está configurado para activarse por nivel, disparará el lector cuando se coloque un objeto ante él, y volverá a dispararlo si no se ha retirado el objeto al acabar la lectura. Esto sucederá de forma continua hasta que se retire el objeto.

### Modo de activaciones enlazadas

Existe la posibilidad de que la ejecución con éxito de algunas operaciones desencadene el inicio de otras. Concretamente, es posible configurar el equipo para ejecutar una lectura RFID tras una lectura de un código de barras, y una escritura RFID (del último código de barras leído en la última etiqueta RFID leída) tras una lectura RFID.

También se puede utilizar esta funcionalidad sin ejecutar la lectura de código de barras, y aportándole en su lugar el EPC que debe ser escrito, con el comando `rewriteTag`.

En ambos casos, es posible configurar un umbral mínimo de RSSI que debe ser cumplido en la lectura, para controlar con mayor precisión qué etiquetas se escriben.

### Modo *streaming*

El lector RFID tiene un modo especial en el que está siempre leyendo. La ausencia de una etiqueta que leer no causará un error; el lector esperará de forma silenciosa hasta que haya una etiqueta en rango de lectura, y cuando la lea, reenviará el EPC leído a través de una conexión especial.

Realizar operaciones que afecten al lector RFID (por ejemplo, lecturas y escrituras manuales) mientras está activado el modo *streaming* resultará en comportamiento indefinido.

Posee cuatro modos de operación, denominados **sesiones**, que determinan la frecuencia con la que se lee una misma etiqueta desde que entra en el campo de lectura:
 - Sesión 0: no se aplica ningún filtro a las etiquetas leídas. Desde que una etiqueta entra en el campo de lectura, y hasta que se retira de éste, se lee tantas veces como el lector pueda.
 - Sesión 1: una vez leída una etiqueta, el lector espera durante un período de tiempo corto (entre 500 milisegundos y 5 segundos) para volverla a leer.
 - Sesiones 2 y 3: una vez leída una etiqueta, no se vuelve a leer hasta que se retira del campo y se introduce en él de nuevo una vez haya transcurrido un período de tiempo (al menos 2 segundos); este período es más corto en sesión 2 y más largo en sesión 3.

---
Es necesario tener en cuenta que el valor de los períodos de espera no está definido con detalle en el estándar EPC y que, en última instancia, los valores reales son establecidos por el fabricante de la etiqueta.

Además de las sesiones, existe otro modo de controlar el tiempo que el lector permanece activo. El modo *streaming* consiste en una secuencia de ciclos de trabajo; en cada ciclo se pasa por una fase de actividad (el lector transmite la señal y recibe las respuestas de las etiquetas) y otra de inactividad (se espera). Es posible modificar cuánto tiempo se permanece en cada una, lo que permite disminuir la energía consumida y mejorar el rendimiento del lector a largo plazo.

Se indica cómo editar todos estos parámetros en la sección denominada Parámetros del lector RFID.

Se puede especificar un valor de timeout para parar la lectura después de un tiempo determinado. Se controla con el parámetro de configuración `cpRFID_STREAMING_OFF_TIMEOUT`. Por defecto, este valor está en segundos; si se necesita mayor precisión, se puede especificar la opción de alta resolución en las opciones del lector RFID (parámetro `cpRFID_READER_OPTIONS`), que hará que el valor se lea como cuartos de segundo.

Cuando expira este temporizador, el equipo lo indicará a través de la conexión de control, con un evento `>StreamingOff`.

## Comunicación Bluetooth

### Búsqueda y emparejado
Por defecto, el nombre Bluetooth del dispositivo es `Minitim-<número de serie>`, y la clave de emparejado es `1234`. El valor del número de serie se puede ver impreso en la parte posterior del Minitim.

### Conexión de control
Para controlar el Minitim a través de Bluetooth, se pueden enviar comandos a través de una aplicación que permita comunicación serie, emulando la comunicación serie sobre Bluetooth. El valor de baudrate seleccionado no influye en el resultado.

![Conexión de control sobre Bluetooth](media_files/comunicacion_bluetooth.png?raw=true "Comunicación sobre Bluetooth")

## Comunicación Wi-Fi o Ethernet

### Configuración inicial Wi-Fi

Si el equipo se inicia realizando una pulsación larga sobre el botón derecho (además de sobre el izquierdo), creará un punto de acceso con nombre `MinitimSoftAp` y clave `12345678`. Para conectar un Minitim a una red Wi-Fi o averiguar su dirección IP, se debe conectar un dispositivo de control al punto de acceso y acceder a la dirección IP `10.10.0.1`, tras lo que se podrá ver una página web de configuración donde se pueden realizar estas acciones.

![Pantalla de configuración de Wi-Fi](media_files/config_wifi_1.png?raw=true "Pantalla de configuración de Wi-Fi")

![Datos de red](media_files/config_wifi_2.png?raw=true "Datos de red")

También es posible configurar la red Wi-Fi a través de la interfaz de control, con el comando `setConfigWifi`, que recibe la SSID de la red y su clave. Para redes abiertas, el campo de clave se deja vacío. Adicionalmente, si se borra el SSID mediante el comando `setConfigWifi&ssid=`, el equipo vuelve a iniciar el modo AP tras el siguiente reinicio.

### Configuración IP

Por defecto el dispositivo configura automáticamente sus interfaces de red activas por DHCP. Este comportamiento puede modificarse mediante los siguientes parámetros de configuración:
```
    # Ethernet
    action=setParam&name=cpIPCONFIG_ETHstr&value=addr,netmask,gateway

    # Wifi (STAtion)
    action=setParam&name=cpIPCONFIG_STAstr&value=addr,netmask,gateway

    # DNS
    action=setParam&name=cpIPCONFIG_DNSstr&value=dns1,dns2,dns_fallback
    # La configuración estática DNS prevalece sobre la configurada por DHCP.

    # Ejemplo:
    action=setParam&name=cpIPCONFIG_ETHstr&value=192.168.0.166,255.255.255.0,192.168.0.100
```

La restauración de valores por defecto mediante `action=setDefaults` *no* restaura la configuración de IP estática. Por el contrario, la restauración de valores por defecto mediante la pulsación larga del botón *sí* restaura la configuración de IP estática.

### Búsqueda de dispositivos

Todos los dispositivos están suscritos al grupo UDP multicast `232.10.11.1` en el puerto `2000`. Si un equipo de control envía el comando hexadecimal `20AA0800` a este punto, los dispositivos MiniTIM y R-FLOW1 conectados a la red local responderán con sus datos.

 - Número de serie
 - Dirección IP
 - Dirección IP de configuración en modo soft AP

Esta es otra forma de obtener la dirección IP, necesaria para establecer una conexión de control sobre Wi-Fi.

![Búsqueda UDP multicast](media_files/busqueda_udp.png?raw=true "Búsqueda de dispositivos Minitim a través de UDP multicast")

### Búsqueda de dispositivos: mDNS

A partir de la versión de firmware 2.1-rflow1 el equipo soporta el descubrimiento local de dispositivos vía mDNS (también conocido como _Bonjour_).

El equipo publica su presencia en la red con un nombre resoluble como `R-Flow1-XXXXXXXXXXXX.local`, siendo `XXXXXXXXXXXX` los 12 dígitos hexadecimales de su dirección MAC base (su MAC Wifi, o MAC de cualquier interfaz módulo 4). Entonces, su nombre puede ser resuelto, en un sistema operativo moderno (ej. Windows 10) directamente por el SO. Así:

![Resolución mDNS](media_files/mdns_ping.png?raw=true "Resolución local de nombres a través de mDNS")

También puede realizarse una búsqueda múltiple de dispositivos en red mediante alguna herramienta estándar de búsqueda mDNS, como por ejemplo [*_mDNS Discovery_*](https://play.google.com/store/apps/details?id=com.mdns_discovery.app&hl=en_US&gl=US). Los detalles de su utilización se reflejan en la siguiente imagen:

![mDNS Discovery](media_files/mdns_discovery.png?raw=true "Búsqueda de dispositivos mediante la APP *_mDNS Discovery_*")

### Conexión de control

La conexión de control sobre Wi-Fi se establece a través de TCP, usando el puerto `1024`. Una vez establecida, se pueden enviar comandos de la misma forma que se relata en la sección Comunicación Bluetooth - Conexión de control.

El software de terminal de libre distribución ***Hercules Setup*** utilizado en estas capturas de pantalla puede descargarse de https://www.hw-group.com/software/hercules-setup-utility.

### Diagnósticos de la conexión

El comando `getDiagsWifi` devuelve una serie de propiedades de la conexión:

 - `ap_info`: datos del punto de acceso Wi-Fi. Incluye:
     - SSID
     - Canal (en el formato `<primario>/<secundario>`)
     - Modo de autenticación:
        - `0`: abierto
        - `1`: WEP
        - `2`: WPA PSK
        - `3`: WPA2 PSK
        - `4`: WPA WPA2 PSK
        - `5`: WPA2 Enterprise
        - `6`: WPA3 PSK
        - `7`: WPA2 WPA3 PSK
        - `8`: WAPI PSK
     - RSSI
     - BSSID
 - `ip_info_wifi`: datos de la conexión Wi-Fi. Incluye:
     - Dirección IP
     - Máscara de red
     - Dirección de la pasarela

    Si no está conectado por Wi-Fi, todos los datos serán `0.0.0.0`.
 - `ip_info_eth`: datos de la conexión Ethernet. Contiene los mismos datos que `ip_info_wifi`.

```
action=getDiagsWifi
ap_info: SSID:  channel: 0/0 authmode: 0 rssi: 0 BSSID: 000000000000
ip_info_wifi: ip: 0.0.0.0 netmask: 0.0.0.0 gw: 0.0.0.0
ip_info_eth: ip: 192.168.0.166 netmask: 255.255.255.0 gw: 192.168.0.100
ip_info_dns: 213.60.205.175 213.60.205.173 0.0.0.0
mac_addr: wifi: 30:83:98:c6:31:90 eth: 30:83:98:c6:31:93
```

## Conexión de control

### Descripción general

Para su manejo remoto y la modificación de su configuración, Minitim acepta una serie de comandos. Su sintaxis es similar a la de una petición HTTP GET:

```
action=comando&nombre_parámetro_1=valor_parámetro_1&nombre_parámetro_2=valor_parámetro_2&...$0D$0A
```

Todos los comandos son terminados por un salto de línea de tipo CRLF.

En las siguientes secciones se describen los detalles de los comandos, agrupados por su tipo.

### Lista de comandos y respuestas

#### Lectura de códigos de barras

| Comando   | Parámetros | Función                                 | Respuesta |
| :-------: | ---------- | --------------------------------------- | --------- |
| `readBc`  | Ninguno    | Dispara el lector de códigos de barras. | El código de barras leído si se ha ejecutado con éxito, y un evento `Timeout` si no. |

#### Lectura y escritura de RFID

| Comando    | Parámetros | Función                                 | Respuesta |
| :--------: | ---------- | --------------------------------------- | --------- |
| `readTag`  | Ninguno    | Dispara el lector de RFID. | La(s) etiqueta(s) RFID leída(s) si se ha ejecutado con éxito, y un evento `Timeout` si no. |
| `writeTag` | `oldEPC` (código antiguo), `newEPC` (código nuevo) | Cambia el código de la etiqueta RFID leída. | El texto `<valor_oldEPC> -> <valor_newEPC>`, donde `<valor_oldEPC>` y `<valor_newEPC>` son los valores de los parámetros proporcionados. Si no se encuentra una etiqueta con valor `<valor_oldEPC>`, se envía un evento `Timeout`. Para que `newEPC` sea válido, debe ser una cadena de dígitos hexadecimales; en caso contrario, el comportamiento es indefinido. |
| `startStreamingMode` | Ninguno | Activa el modo de streaming del lector RFID. | Un evento `Done` |
| `stopStreamingMode` | Ninguno | Desactiva el modo de streaming del lector RFID. | Un evento `Done` |
| `rewriteTag` | `newEPC` (código nuevo) | Lee una etiqueta RFID y, si se lee con un RSSI superior al umbral configurado, le escribe el valor `newEPC`. Requiere que el parámetro de configuración `cpLINKED_TRIGGERS` tenga como valor `2` o `3`. | La respuesta de `readTag`, seguida de la de `writeTag`. |
| `getTimestamp | Ninguno | Devuelve la timestamp actual; el tiempo en milisegundos desde que el equipo se alimentó. | El evento `>Timestamp <timestamp>` |
___

Existen otras restricciones para que el nuevo EPC sea válido, que el equipo gestiona para poder dar una respuesta definida. Un EPC debe tener un número de caracteres que sea múltiplo de 4; si el nuevo EPC no cumple esto, se añadirá el carácter `0` por la izquierda hasta que el número de caracteres sea múltiplo de 4. De la misma forma, si se intenta escribir un EPC de una longitud mayor a 32 caracteres, se escribirán los 32 primeros.

#### Acceso y modificación de los parámetros de configuración

| Comando       | Parámetros | Función | Respuesta |
| :-----------: | ---------- | ------- | --------- |
| `getConfig`   | Ninguno    | Imprime el valor de todos los parámetros de configuración. | Todos los parámetros de configuración, en el formato `<nombre>:<valor>`. |
| `setDefaults` | Ninguno    | Restaura los valores por defecto de la configuración. | Todos los parámetros de configuración con sus nuevos valores, en el formato `<nombre>:<valor>`. |
| `getParam`   | `name` (nombre del parámetro) | Imprime el valor de un parámetro. | El parámetro de nombre `name` y su valor, en el formato `<nombre>:<valor>`. |
| `setParam`   | `name` (nombre del parámetro), `value` (nuevo valor) | Cambia el valor de un parámetro. | El parámetro de nombre `name` y su nuevo valor `value`, en el formato `<nombre>:<valor>`. |
| `setParamVol`   | `name` (nombre del parámetro), `value` (nuevo valor) | Cambia el valor de un parámetro de forma volátil; el valor anterior se restaurará tras un reinicio. Todos los parámetros con valores volátiles irán marcados con el símbolo `*`. | El parámetro de nombre `name` y su nuevo valor `value`, en el formato `<nombre>:<valor>`, más una marca indicando que es volátil. |
| `commitConfig`   | Ninguno | Convierte todos los parámetros volátiles en no volátiles. | Todos los parámetros de configuración, en el formato `<nombre>:<valor>`. |
| `getRRParam`   | `name` (nombre del parámetro) | Imprime el valor de un parámetro del lector RFID. | El parámetro de nombre `name` y su valor, en el formato `<nombre>:<valor>`. |
| `setRRParam`   | `name` (nombre del parámetro), `value` (nuevo valor) | Cambia el valor de un parámetro del lector RFID. | El parámetro de nombre `name` y su nuevo valor `value`, en el formato `<nombre>:<valor>`. |
| `setConfigWifi` | `ssid`, `password` | Configura los parámetros del punto de acceso Wi-Fi. Si ssid se deja sin asignar, se activa modo SoftAP. | El texto `>WifiConfig: <nuevo SSID>` |
| `getDiagsWifi` | Ninguno | Devuelve diagnósticos de la conexión IP. | Los datos descritos en la sección "Diagnósticos de la conexión", en formato `<clave>: <valor>` |
| `ota` | `image` (URL de donde se descargará la nueva imagen de firmware) | Actualiza el firmware del dispositivo. | `>OTA:0` antes de comenzar la actualización, y `>OTA: <código de resultado>` tras finalizarla. |

___
Todos los parámetros del lector RFID son volátiles y vuelven a sus valores por defecto tras un reinicio.

##### Ejemplos

Esta secuencia de comandos de ejemplo realiza el proceso de modificación del EPC de un tag RFID al valor de un código de barras leído previamente.

Notas:

 - Las secuencias entre corchetes representan las respuestas recibidas desde el equipo.
 - En los comandos de debajo no se explicitan los caracteres `<CR><LF>` de fin de línea que suceden a cada uno.

###### Lectura de código de barras

```
action=readBc
[< 3018030604904]
```

###### Lectura de tag RFID

```
action=readTag
[< EE112233445566778899AABB,-37]
```

###### Cambio del EPC del tag RFID

```
action=writeTag&oldEPC=EE112233445566778899AABB&newEPC=000000000003018030604904
[< EE112233445566778899AABB->000000000003018030604904]
```

###### Modificación de la potencia de lectura RFID

```
action=setRRParam&name=/reader/radio/readPower&value=1800
[< /reader/radio/readPower=1800]
```

#### Otros comandos
| Comando    | Parámetros | Función              | Respuesta |
| :--------: | ---------- | -------------------- | --------- |
| `reset`    | Ninguno    | Reinicia el Minitim. | Ninguna   |
| `poweroff` | Ninguno    | Apaga el Minitim.    | Ninguna   |

### Tipos de eventos

Los dispositivos Minitim tienen la capacidad de enviar información a través de la conexión de control de forma asíncrona (sin que sea una respuesta a un comando), o de forma síncrona.

| Evento          | Función                            |
| :-------------: | -----------------------------------|
| `BatteryStatus` | Indica el estado de la batería; se envía periódicamente. Los parámetros del evento tienen la forma: `nivelDeBatería [1-10]`, `enCarga [0-1]`, `cargaCompletada[0-1]`, `tensiónDeBatería[mV]`|
| `Timeout`       | Se envía cuando se ha solicitado la lectura de un código de barras o una etiqueta RFID, pero los lectores no detectan ninguno. | Síncrono |
| `Trigger`       | Se envía cuando se dispara alguno de los lectores.       |
| `Poweroff`      | Se envía cuando se apaga el dispositivo.                 |
| `Done`          | Se envía cuando se procesa un comando que no devuelve una respuesta. |
| `Busy`          | Se envía cuando se solicita una operación a un lector que está ocupado (por ejemplo, si se envía un `readTag` mientras está activo el modo *streaming* RFID).  |

Todos los mensajes de evento van precedidos por el símbolo `>`.

## Parámetros de configuración

A continuación se describen los parámetros que permiten configurar el comportamiento de cada Minitim.

Todos sus nombres van precedidos por el texto `cp` (**c**onfiguration **p**arameter), y por otro texto que depende de la funcionalidad con la que se relacionan.

 - `BC` para lectura de códigos de barras.
 - `RFID` para lectura o escritura de códigos RFID.
 - `BT` para comunicación Bluetooth.
 - Los parámetros globales no llevan ningún otro prefijo.

Los nombres de los parámetros que aceptan como valor cualquier cadena de texto van sucedidos por el sufijo `str`.

### Lectura de códigos de barras

| Nombre            | Función | Posibles valores | Valor por defecto |
| :---------------: | ------- | ---------------- | ----------------- |
| `cpBC_READ_TIMEOUT` | Tiempo de espera para lectura de códigos de barras | Enteros positivos (segundos) | 2 segundos |
| `cpBC_TRIGGER_MODE` | Modos en los que se dispara la lectura de códigos de barras | Una máscara de 5 bits en la que cada bit activo indica que el correspondiente modo de activación de la operación de lectura de códigos de barras está habilitado. Bit 0: activable por pulsación corta del botón OK. Bit 1: activable por radar. Bit 2: activable por control remoto (activo aunque se ponga a 0). Bit 3: activable por pulsación larga del botón OK. Bit 4: activable por pulsación corta del botón Cancelar. Bit 5: activado continuamente. | 4 (activable por control remoto, pero no radar ni botón)|
| `cpBC_RADAR_RANGE` | Rango de detección del radar | Los valores 0 (rango corto, sobre 30 cm) o 1 (rango largo, sobre 60 cm) | 0 (rango corto) |
| `cpBC_RADAR_TRIGGER_OPERATION` | Modo de activación del radar | Los valores 0 (activación por nivel) y 1 (activación por flanco de subida) | 1 (activación por flanco) |
| `cpBC_PREFIXstr` | Prefijo para añadir a los valores leídos a través del lector de códigos de barras | Cualquier string | Vacío |
| `cpBC_SUFFIXstr` | Sufijo para añadir a los valores leídos a través del lector de códigos de barras | Cualquier string | Vacío |

Para añadir caracteres especiales , será necesario introducir la secuencia de escape `\` seguida del código hexadecimal (de dos cifras) del carácter. Por ejemplo, `\65` imprime la letra `e`, y la secuencia `\0A\0D` imprime los caracteres `<CR><LF>`. El carácter `\` se escribiría `\\`. Si se escribe un `\` seguido por dos caracteres que no son dígitos hexadecimales, la cadena no se modifica; esto es, escribir `\xy` generaría `\xy`.

### Lectura de RFID

| Nombre            | Función | Posibles valores | Valor por defecto |
| :---------------: | ------- | ---------------- | ----------------- |
| `cpRFID_OP_TIMEOUT` | Tiempo de espera para lectura de códigos RFID | Enteros positivos (segundos) | 1 segundo |
| `cpRFID_TRIGGER_MODE` | Modos en los que se dispara la lectura RFID | Una máscara de 5 bits en la que cada bit activo indica que el correspondiente modo de activación de la operación de lectura RFID está habilitado. Bit 0: activable por pulsación corta del botón OK. Bit 1: activable por radar. Bit 2: activable por control remoto (activo aunque se ponga a 0). Bit 3: activable por pulsación larga del botón OK. Bit 4: activable por pulsación corta del botón Cancelar. | 1 (activable por botón, pero no por radar) |
| `cpRFID_WRITE_TRIGGER_MODE` | Modos en los que se dispara la escritura RFID | Una máscara de 5 bits en la que cada bit activo indica que el correspondiente modo de activación de la operación de escritura RFID está habilitado. Bit 0: activable por pulsación corta del botón OK. Bit 1: activable por radar. Bit 2: activable por control remoto (activo aunque se ponga a 0). Bit 3: activable por pulsación larga del botón OK. Bit 4: activable por pulsación corta del botón Cancelar. | 16 (activable sólo por pulsación corta del botón Cancelar) |
| `cpRFID_PREFIXstr` | Prefijo para añadir a los valores leídos a través del lector de códigos RFID | Cualquier string | Vacío |
| `cpRFID_SUFFIXstr` | Sufijo para añadir a los valores leídos a través del lector de códigos RFID | Cualquier string | El string `,%s`; `%s` imprime ciertos metadatos de la lectura RFID. |
| `cpLINKED_TRG_RSSI_THR` | Umbral de RSSI que las lecturas RFID deben cumplir para que funcione el modo de activaciones enlazadas. | Enteros positivos o negativos. | -100 dBm |
` `cpRFID_STREAMING_OFF_TIMEOUT` | Tiempo de espera para parar el modo *streaming* | Enteros positivos (segundos o cuartos de segundo, según esté activada o no la opción de alta resolución en `cpRFID_READER_OPTIONS`) o `0` (desactivado). | Desactivado |
| `cpRFID_READER_OPTIONS` | Modifica diversas opciones relacionadas con el lector RFID o lo que se hace con sus lecturas. | Máscara de bits en la que cada bit activo habilita una opción. Bit 0: deshabilitar ahorro de energía del lector \*. Bit 1: temporizadores de alta resolución en el modo *streaming*. Bit 4: emisión de lecturas *dummy* (EPC, RSSI y antena a 0) al terminar un *duty cycle* \*\*. El resto de bits se usan para opciones exclusivas del R-FLOW1. | `1` (ahorro de energía desactivado, resto de opciones desactivadas) |

#### Parámetros del lector RFID

Se leen con el comando `getRRParam` y se establecen con el comando `setRRParam`, descritos anteriormente. Es necesario tener en cuenta que ninguno de los parámetros del lector RFID es persistente; tras el reinicio, todos recuperan sus valores por defecto.

| Nombre            | Función | Posibles valores | Valor por defecto |
| :---------------: | ------- | ---------------- | ----------------- |
| `/reader/read/asyncOffTime` | Período de tiempo durante el lector que está inactivo durante el ciclo de trabajo | Enteros positivos (milisegundos) | 0 milisegundos |
| `/reader/read/asyncOnTime` | Período de tiempo durante el lector que está activo durante el ciclo de trabajo | Enteros positivos (milisegundos) | 250 milisegundos |
| `/reader/gen2/session` | Sesión del lector; se detalla en el apartado Modo *streaming* | Los valores `S0`, `S1`, `S2` y `S3` | `S0` |
| `/reader/radio/readPower` | Potencia de transmisión de señal para las operaciones de lectura | Enteros (centidBm) | 2300 centiDbm |

El comando `setRRParam` permite también llevar a cabo operaciones RFID avanzadas, que se describen en detalle en [este documento separado](rfid_operations.md).

### Parámetros Bluetooth

| Nombre            | Función | Posibles valores | Valor por defecto |
| :---------------: | ------- | ---------------- | ----------------- |
| `cpBT_PINCODEstr` | Clave de emparejamiento Bluetooth | Cualquier string | El string `1234` |
| `cpBT_DEVICE_NAMEstr` | Nombre Bluetooth del dispositivo | Cualquier string | El string `Minitim-%s`; `%s` imprime el número de serie. |

### Parámetros generales

| Nombre            | Función | Posibles valores | Valor por defecto |
| :---------------: | ------- | ---------------- | ----------------- |
| `cpRD_UNSOEVENT_MASK` | Conjunto de eventos sobre los que un Minitim envía información al dispositivo de control remoto | Máscara de bits en la que cada bit activo indica que se envía información sobre el correspondiente evento. Bit 0: `BatteryStatus`. Bit 1: `Trigger`. Bit 2: `Timeout`. Bit 3: `Poweroff`. Bit 5: `Done`. Bit 6: `Busy`. | 255 (se envía información sobre todos los eventos) |
| `cpLINKED_TRIGGERS` | Modos activados de enlazamiento de operaciones | Máscara de bits en la que cada bit activo indica que la ejecución de una operación con éxito desencadena el inicio de otra operación. Bit 0: la lectura de código de barras desencadena una lectura RFID. Bit 1: la lectura RFID desencadena una escritura RFID (del último código de barras leído en la última etiqueta RFID leída). | 0 (ninguna operación causa el inicio de otra) |
| `cpVIBRATION_AS_FEEDBACK` | Activación de la vibración como *feedback* al usuario de que una operación se ejecutó con éxito | Máscara de bits en la que cada bit activo indica que está activada la vibración como *feedback* al usuario. Bit 0: activada para lectura de códigos de barras. Bit 1: activada para lectura RFID. Bit 2: activada para escritura RFID. | 0 (nunca se vibra como *feedback* al usuario) |
| `cpSOUND_AS_FEEDBACK` | Activación de los pitidos como *feedback* al usuario de que una operación se ejecutó con éxito | Máscara de bits en la que cada bit activo indica que está activado el sonido como *feedback* al usuario. Bit 0: activado para lectura de códigos de barras (está siempre activo, aunque este bit se ponga a 0). Bit 1: activado para lectura RFID. Bit 2: activado para escritura RFID. | 7 (el dispositivo pita tras cualquier operación exitosa) |

## Autorun

El parámetro `cpAUTORUNstr` acepta como valor una lista de comandos, que se ejecutarán automáticamente en el inicio del dispositivo. Los comandos deben ir separados por el carácter `|` y no deben incluir la secuencia de terminación `$0D$0A`.

```
action=setParam&name=cpAUTORUNstr&value=action=setParam&name=cpRFID_STREAMING_OFF_TIMEOUT&value=10|action=setParam&name=cpRFID_OP_TIMEOUT&value=5
```

Se puede ejecutar la lista de comandos de startup a mano en cualquier momento, usando el comando `autorun`, que no acepta parámetros.

```
action=autorun
```

Es útil para, por ejemplo, simular persistencia en los parámetros del lector RFID (que siempre son volátiles), colocando en `cpAUTORUNstr` los comandos `setRRParam`.

## Conexión de *streaming*

Si está activado el modo *streaming* del lector RFID, se pueden visualizar los EPCs leídos estableciendo una conexión TCP en el puerto 1027 del dispositivo. El formato en el que se visualizan es el mismo que el que se haya configurado para las lecturas síncronas (a través de la definición del prefijo y el sufijo).

La conexión no está ligada a la activación del modo *streaming*, de modo que este puede activarse y desactivarse mientras la conexión esté abierta, y la conexión puede cortarse sin afectar a la funcionalidad del lector.

### Ejemplo de uso del modo *streaming*

Se describen a continuación los pasos para utilizar el modo de lectura continua. Se mantiene la sintaxis utilizada en la sección de ejemplos anterior.

 1. Se establece la conexión de control con el puerto 1024 del dispositivo.

 2. Se configura la sesión (opcionalmente).

 ```
 action=setRRParam&name=/reader/gen2/session&value=S0
 [/reader/gen2/session=S0]
 ```

 3. Se configura la longitud del período de actividad (opcionalmente).

 ```
 action=setRRParam&name=/reader/read/asyncOnTime&value=500
 [/reader/read/asyncOnTime=500]
 ```
 4. Se configura la longitud del período de inactividad (opcionalmente).

 ```
 action=setRRParam&name=/reader/read/asyncOffTime&value=1000
 [/reader/read/asyncOffTime=1000]
 ```

 5. Se configura la potencia de lectura (opcionalmente).

 ```
 action=setRRParam&name=/reader/radio/readPower&value=1800
 [/reader/radio/readPower=1800]
 ```

 6. Se establece la conexión de *streaming* para recibir los EPC leídos en el dispositivo de control.

 ![Establecimiento de la conexión de streaming](media_files/conexion_streaming.png)

 7. Se envía el comando de inicio a través de la conexión de control.

 ```
 action=startStreamingMode
 ```

 8. Si hay etiquetas RFID en el campo de lectura del dispositivo, se recibirán sus EPC a través de la conexión de *streaming*, formateados con el prefijo y sufijo especificados.

 ![Recepción de EPC a través de la conexión de streaming](media_files/resultado_streaming.png)

 9. En el momento en que se desee parar, se envía el comando de parada.

 ```
 action=stopStreamingMode
 ```

## Actualización OTA

Los equipos R-FLOW1 y MiniTIM tienen la capacidad de actualizar su propio firmware a través de la red, especificándole la URL de la nueva imagen de firmware a través del comando `ota`. 

```
action=ota&image=https://www.dev0-pdm.dyndns.org/webbt/images/rflow1-v1.5-hwversion7.bin
```

Devolverá el texto `>OTA: 0` antes de iniciar la actualización, y el texto `>OTA: <código de resultado>` tras terminarla (código igual a 1) o encontrar un error (código inferior a 0).

| Código | Significado |
| :----: | :---------- |
| `1`    | Éxito       |
| `-1`   | No se ha podido inicializar la conexión HTTP. |
| `-2`   | No se ha podido establecer la conexión HTTP. |
| `-3`   | Error de recepción de datos. |
| `-4`   | La versión de firmware es igual a una versión ya marcada como no válida. |
| `-5`   | La versión de firmware es igual a la actual. |
| `-6`   | No se ha podido iniciar la escritura de la nueva imagen. |
| `-7`   | La imagen recibida no tiene el tamaño adecuado. |
| `-8`   | Error durante la escritura de la nueva imagen en memoria. |
| `-9`   | No se ha podido finalizar la escritura de la nueva imagen. |
| `-10`  | No se ha podido establecer la partición de arranque. |

La conexión debe ser HTTPS. Tras actualizar con éxito, el dispositivo se reiniciará.

Si el nuevo firmware no puede arrancar, el equipo volverá de forma autónoma a la versión anterior.

Las versiones de firmware y de hardware están, respectivamente, contenidas en los parámetros `cpVERSIONstr` y `cpHWVERSIONstr`. No son modificables por el usuario.