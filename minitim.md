# Detalles MiniTIM

## Interfaz de usuario: botones y LEDs

La interfaz física de usuario cuenta con dos botones a modo de entrada, y cuatro LEDs, un buzzer y un vibrador como salida.

El equipo se enciende realizando una pulsación larga sobre el botón izquierdo, hasta que el dispositivo vibra. El apagado se realiza de la misma manera.

La funcionalidad de los botones es configurable. Se contemplan tres acciones sobre los botones:

 - Pulsación corta sobre el botón derecho o de disparo.
 - Pulsación larga sobre el botón derecho.
 - Pulsación corta sobre el botón izquierdo. La pulsación larga sobre el botón izquierdo no es configurable, ya que está reservada para el apagado del dispositivo.

Cada una de estas acciones puede configurarse para causar el disparo de:

 - una lectura de un código de barras,
 - una lectura RFID,
 - o una escritura en la última etiqueta RFID leída del valor último código de barras leído. Si se lanza esta funcionalidad sin haber leído antes ninguna etiqueta RFID o código de barras, el equipo indicará el fallo con un pitido grave.

Existe un LED de color rojo y uno de color verde en cada lado del equipo. Su significado se detalla a continuación.

 - LED verde izquierdo: estado de red.
    - Si parpadea lentamente, el equipo está esperando una conexión TCP.
    - Si no parpadea, está conectado por TCP a un equipo controlador.
    - Si parpadea rápidamente, emite un punto de acceso a una red de configuración. Los detalles sobre este punto se pueden ver en la sección Configuración inicial.
 - LED rojo izquierdo: indicador de batería baja.
 - LED verde derecho: indicador de activación del lector RFID.
 - LED rojo derecho: no utilizado.

___
No es necesario indicar la activación del lector de códigos de barras, ya que para su operación emite una luz visible por el usuario.