# Detalles R-FLOW1

## Interfaz de usuario

La interfaz física de usuario comprende un LED verde, un buzzer y un botón.

El botón está a la izquierda de la parte delantera. Si se realiza una pulsación larga, restaura los parámetros de configuración por defecto.

El dispositivo cuenta con varios conectores en la parte delantera. A la derecha del botón, y de izquierda a derecha, se encuentran:

    - Conector de antena externa
    - Conector de alimentación
    - Puerto USB tipo B
    - Puerto Ethernet

El LED, que está situado en la parte izquierda, tiene los siguientes modos:

    - Parpadeo lento: esperando una conexión de control.
    - Parpadeo rápido: conectado, por cualquiera de las interfaces (USB, TCP...) a un equipo controlador.
    - Parpadeo muy rápido: modo streaming activado.

## Entradas y salidas digitales

El equipo tiene un conector de entradas y salidas digitales en la parte izquierda. Es posible actuar sobre las salidas con el comando `setIO`, y leer las entradas con el comando `getIO`. Su sintaxis se describe en el apartado de comandos y respuestas.

La actuación sobre las entradas y salidas digitales es incompatible con el modo *streaming*.

### Actuación sobre las salidas digitales como *feedback*

El parámetro `cpGPIO_AS_FEEDBACK` controla la actuación sobre las salidas digitales como feedback de un evento. Se consideran los siguientes eventos:
    - Activación (trigger) de una lectura
    - Recepción de datos de una lectura (esto es, la lectura no ha dado timeout)
    - Timeout de lectura

Cada salida puede encenderse, apagarse o cambiarse de estado (sólo disponible para el evento trigger) como consecuencia del evento. No vuelven al estado anterior automáticamente.

El parámetro es una máscara de bits en la que el byte n-ésimo gobierna la salida n-ésima, según la siguiente estructura:
    - Bit 0: encender la salida en trigger
    - Bit 1: apagar en trigger
    - Bit 2: cambiar la salida de estado en trigger
    - Bit 3: encender en lectura
    - Bit 4: apagar en lectura
    - Bit 5: encender en timeout
    - Bit 6: apagar en timeout

## Modos USB

El R-FLOW1 tiene una interfaz USB que proporciona tres canales de comunicación:

 - **Puerto SPP de debug:** emulación de puerto serie que se utiliza para enviar logs de debug y cargar nuevo firmware.
 - **Dispositivo HID para emisión de lecturas:** utiliza emulación de teclado para enviar a un host los datos de las tags RFID leídas. Se puede controlar qué lecturas se envían con el parámetro `cpRFID_READER_OPTIONS`. 
 - **Puerto SPP de comunicación:** puede conmutar entre dos modos:
     - **Emisión de lecturas:** similar al funcionamiento del canal HID. Es el modo por defecto.
     - **Interfaz de control:** permite enviar comandos y recibir respuestas, de la misma forma que se hace a través de la conexión TCP. Hay que habilitarla utilizando una opción del parámetro `cpRFID_READER_OPTIONS`. Si está habilitada, el canal conmuta a este modo cuando se activan las señales DTR y RTS.

## Nota sobre el modo de activación continua

El modo de activación continua, que está habilitado cuando `cpRFID_TRIGGER_MODE` tiene valor 32 o superior, ejecuta una lectura RFID siempre que puede, sin necesidad de ningún input. Es similar al modo *streaming*, con la diferencia de que el modo *streaming* es incompatible con ciertas operaciones, mientras que el modo de activación continua permite intercalar cualquier operación entre las lecturas.

## Lista de comandos y respuestas

| Comando   | Parámetros | Función                                 | Respuesta |
| :-------: | ---------- | --------------------------------------- | --------- |
| `setIO`   | `name` (número de salida), `value` (nuevo valor) | Pone la salida indicada al valor indicado. | La respuesta del comando `getIO`. Por lo general, el comando `setIO` no influye en este valor. |
| `getIO`   | Ninguno    | Devuelve el estado de las entradas.     | `>IO: <entradas>`; `<entradas>` es una máscara de 32 bits, donde el bit 0 representa el estado de la entrada 0 del conector, y el bit 1 el de la entrada 1. El valor del resto de bits no es relevante. |

## Parámetros de configuración

| Nombre            | Función | Posibles valores | Valor por defecto |
| :---------------: | ------- | ---------------- | ----------------- |
| `cpRFID_READER_OPTIONS` | Modifica diversas opciones relacionadas con el lector RFID o lo que se hace con sus lecturas. | Máscara de bits en la que cada bit activo habilita una opción. Bit 0: deshabilitar ahorro de energía del lector \*. Bit 1: temporizadores de alta resolución en el modo *streaming*. Bit 2: reenviar lecturas manuales por HID. Bit 3: reenviar lecturas de streaming por HID. Bit 4: emisión de lecturas *dummy* (EPC, RSSI y antena a 0) al terminar un *duty cycle* \*\*. Bit 5: interfaz de control por USB. | `1` (ahorro de energía desactivado, resto de opciones desactivadas) |
| `cpGPIO_AS_FEEDBACK` | Controla la actuación sobre salidas digitales cuando ocurren eventos específicos. | Máscara de bits en la que cada bit activo habilita una combinación de evento y estado del GPIO. Su estructura se describe en la sección "Actuación sobre las salidas digitales como *feedback*". | `0` (no se actúa sobre ninguna salida) |
___

\* El ahorro de energía del lector se habilita con el valor 0 y se deshabilita con el valor 1, al contrario que el resto de opciones.

\*\* Sólo se utiliza cuando las siguientes opciones del lector tienen valores específicos: `/reader/read/asyncOffTime` distinto de 0, y `/reader/tagReadData/recordHighestRssi` igual a `true`. 

## Configuración rápida para modo de escritorio con emulación de teclado HID

Con los siguientes comandos se consigue configurar el lector para lectura continua de corto alcance con entrega de lecturas vía emulación de teclado:

```
action=setParam&name=cpRFID_SUFFIXstr&value=\0D\0A
action=setParam&name=cpRFID_TRIGGER_MODE&value=32
action=setParam&name=cpRFID_READER_POWER&value=1000
action=setParam&name=cpRFID_READER_OPTIONS&value=15
```
