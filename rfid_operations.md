
# Operaciones RFID

## Selección de antena (operaciones de lectura)

Mediante la configuración de un *ReadPlan* con anterioridad a la invocación de un comando`readTag` puede seleccionarse la/s antena/s a través de la cual se realizará la operación.

Las antenas disponibles en cada producto son:

- Minitim (lector M6e-Nano)

    - `1`: antena integrada (interna) [antena activa por defecto]

- Rflow1 (lector M6e-Micro)

    - `1`: antena integrada (interna) [antena activa por defecto]

    - `2`: antena externa (conector de antena)

Ejemplos:
```
# Selección de antena externa en Rflow1
action=setRRParam&name=/reader/read/plan&value=SimpleReadPlan:[Antennas=[2],Protocol=GEN2,Filter=null,Op=null,UseFastSearch=false,Weight=1000]

# Selección de ambas antenas en Rflow1
action=setRRParam&name=/reader/read/plan&value=SimpleReadPlan:[Antennas=[1,2],Protocol=GEN2,Filter=null,Op=null,UseFastSearch=false,Weight=1000]
```

## Selección de antena (otras operaciones)

La selección de antena/s mediante la edición del *ReadPlan* se aplica solamente al comando `readTag`. Para el resto de operaciones debe seleccionarse una única antena del siguiente modo:
```
# Seleccionamos la antena 2 para las operaciones de escritura "writeTag"
action=setRRParam&name=/reader/tagop/antenna&value=2
```

## Filtrado de tags

Mediante la manipulación del *ReadPlan* también puede establecerse un filtro genérico de tags, para acotar una operación a un subconjunto de los tags en rango. 

Puede realizarse un filtrado simple por EPC, en el que se especifica el comienzo de los EPC's deseados mediante una cadena de longitud arbitraria (pero múltiplo de 8 bits), como en el siguiente ejemplo:
```
# Configuramos un filtro para leer tags cuyo EPC empieza por E2
action=setRRParam&name=/reader/read/plan&value=SimpleReadPlan:[Antennas=[1],Protocol=GEN2,Filter=TagData:[EPC=E2],Op=null,UseFastSearch=false,Weight=1000]

# 
action=readTag
>Trigger
E2806890000050019941D991
E2806890000040019941D990
```

También puede aplicarse un filtro complejo mediante el enmascaramiento de una zona arbitraria del mapa de memoria del tag, como aquí:

```
# Configuramos un filtro para leer tags (el tag) cuyo EPC es E2806890000040019941D990
# Consideraciones:
# - el selector de bancos puede tomar los valores: Reserved, EPC, TID, User
# - el EPC de un tag EPC Gen2 reside a partir del offset 32 de su banco de memoria EPC
# - BitLength puede tomar cualquier valor, pero Mask ha de definir un número entero de bytes (longitud par)
action=setRRParam&name=/reader/read/plan&value=SimpleReadPlan:[Antennas=[2],Protocol=GEN2,Filter=Gen2.Select:[Invert=false,Bank=EPC,BitPointer=32,BitLength=96,Mask=E2806890000040019941D990],Op=null,UseFastSearch=false,Weight=1000]

# 
action=readTag
>Trigger
E2806890000040019941D990
```

El filtro por defecto (que no realiza filtrado) se restaura igualando a `null` el parámetro:

```
# Restauración del filtro por defecto (sin filtrado)
action=setRRParam&name=/reader/read/plan&value=SimpleReadPlan:[Antennas=[1],Protocol=GEN2,Filter=null,Op=null,UseFastSearch=false,Weight=1000]
```

A modo de referencia se incluye el mapa de memoria de un tag EPC Gen2:

![Mapa de memoria de un tag EPC Gen2](media_files/epcgen2memorymap.png)

Se ha comprobado que la configuración de un MultiReadPlan con 2 filtros del mismo tipo (TagData + TagData o Gen2.Select + Gen2.Select) no proporciona los resultados deseados. Sólo el filtro del primer SimpleReadPlan tiene efecto. Esto es independiente de las antenas a las que apliquen los filtros.

Sin embargo, se han obtenido resultados satisfactorios para leer únicamente 2 patrones de EPC's combinando un filtro de cada tipo. Debajo se muestran ejemplos de configuración:

```
# Sólo lee los 0048
action=setRRParam&name=/reader/read/plan&value=MultiReadPlan:[SimpleReadPlan:[Antennas=[1],Filter=TagData:[EPC=0048]],SimpleReadPlan:[Antennas=[1],Filter=TagData:[EPC=71],Weight=1]]]

# Configura ambos SimpleReadPlan's con el segundo filtro (el 71xx). Puede que sea por causa del firmware (tmr_load_save_configuration)
action=setRRParam&name=/reader/read/plan&value=MultiReadPlan:[SimpleReadPlan:[Antennas=[1],Filter=Gen2.Select:[Invert=false,Bank=EPC,BitPointer=32,BitLength=8,Mask=00]],SimpleReadPlan:[Antennas=[1],Filter=Gen2.Select:[Invert=false,Bank=EPC,BitPointer=32,BitLength=8,Mask=71],Weight=1]]]
> /reader/read/plan=MultiReadPlan:[SimpleReadPlan:[Antennas=[1],Protocol=GEN2,Filter=Gen2.Select:[Invert=false,Bank=EPC,BitPointer=32,BitLength=8,Mask=71],Op=null,UseFastSearch=false,Weight=1],SimpleReadPlan:[Antennas=[1],Protocol=GEN2,Filter=Gen2.Select:[Invert=false,Bank=EPC,BitPointer=32,BitLength=8,Mask=71],Op=null,UseFastSearch=false,Weight=1]]

# Funciona según lo esperado y lee ambos tipos, el 0048 y el 71
action=setRRParam&name=/reader/read/plan&value=MultiReadPlan:[SimpleReadPlan:[Antennas=[1],Filter=TagData:[EPC=0048]],SimpleReadPlan:[Antennas=[1],Filter=Gen2.Select:[Invert=false,Bank=EPC,BitPointer=32,BitLength=8,Mask=71],Weight=1]]]

# Filtrado de lectura por TID (posibilita por ejemplo reescribir un tag con EPC duplicado)
action=setRRParam&name=/reader/read/plan&value=SimpleReadPlan:[Antennas=[1],Protocol=GEN2,Filter=Gen2.Select:[Invert=false,Bank=TID,BitPointer=0,BitLength=96,Mask=E28011702000050BB8930AB0],Op=ReadData:[Bank=TID,WordAddress=0,Len=6]]$0D$0A
```

## Lectura/escritura arbitraria en memoria

El parámetro *Op* del *ReadPlan* permite seleccionar una operación de lectura o escritura que se ejecutará en la siguiente invocación de `ReadTag`. Deben tenerse en cuenta las siguientes consideraciones para estas operaciones:

- La lectura y escritura de la memoria se realiza con granularidad de *word* (16 bits).

- No todos los tags implementan el banco de memoria de usuario. Consultar la especificación proporcionada por el fabricante para comprobar su disponibilidad.

- En la operación de escritura, el propio equipo invierte adecuadamente los bytes dentro de cada word para que la secuencia hexadecimal escrita se corresponda con la leída posteriormente. Esta inversión produce sin embargo el efecto de que el *ReadPlan* visualizado una vez modificado tendrá los bytes invertidos.

- La operación de lectura o escritura puede seleccionarse de manera independiente a la selección de un filtro.

- Los datos solicitados en una operación de lectura se vuelcan a continuación del EPC del tag, separados por un carácter `/`.

- El tamaño máximo admisible en las operaciones de escritura mediante este método es de 32 bytes / 16 words.

```
# Lectura del banco TID, para cualquier tag
action=setRRParam&name=/reader/read/plan&value=SimpleReadPlan:[Antennas=[1],Protocol=GEN2,Filter=null,Op=ReadData:[Bank=TID,WordAddress=0,Len=6],UseFastSearch=false,Weight=1000]
#
action=readTag
>Trigger
303602C3F45E035000000001/E280699520004000FDE1553F,-20,1,251892986

# Lectura arbitraria de datos del banco de memoria de usuario, para un tag concreto
action=setRRParam&name=/reader/read/plan&value=SimpleReadPlan:[Antennas=[1],Protocol=GEN2,Filter=Gen2.Select:[Invert=false,Bank=EPC,BitPointer=32,BitLength=96,Mask=300833B2DDD9014000000000],Op=ReadData:[Bank=User,WordAddress=0,Len=16],UseFastSearch=false,Weight=1000]
#
action=readTag
>Trigger
300833B2DDD9014000000000/1234567800000000000000000000000000000000000000000000000000000000

# Escritura arbitraria de datos del banco de memoria de usuario, para un tag concreto
action=setRRParam&name=/reader/read/plan&value=SimpleReadPlan:[Antennas=[1],Protocol=GEN2,Filter=Gen2.Select:[Invert=false,Bank=EPC,BitPointer=32,BitLength=96,Mask=300833B2DDD9014000000000],Op=WriteData:[Bank=User,WordAddress=2,Data=AABBCCDDEEFF],UseFastSearch=false,Weight=1000]
#
action=readTag
>Trigger
300833B2DDD9014000000000

# Lectura de confirmación
action=setRRParam&name=/reader/read/plan&value=SimpleReadPlan:[Antennas=[1],Protocol=GEN2,Filter=Gen2.Select:[Invert=false,Bank=EPC,BitPointer=32,BitLength=96,Mask=300833B2DDD9014000000000],Op=ReadData:[Bank=User,WordAddress=0,Len=16],UseFastSearch=false,Weight=1000]
#
action=readTag
>Trigger
300833B2DDD9014000000000/12345678AABBCCDDEEFF00000000000000000000000000000000000000000000

# Restauración del filtro por defecto (sin filtrado)
action=setRRParam&name=/reader/read/plan&value=SimpleReadPlan:[Antennas=[1],Protocol=GEN2,Filter=null,Op=null,UseFastSearch=false,Weight=1000]

```

## Sufijo RFID

En el sufijo RFID pueden incluirse metadatos relacionados con las lecturas:

- RSSI o cobertura de la señal, en dBm

- Antena a través de la cual se ha leído esa etiqueta (1 = antena interna, 2 = antena externa). Es útil cuando el equipo se ha configurado para leer con las dos antenas.

- Timestamp de lectura: el tiempo transcurrido, en milisegundos, desde que el equipo fue **alimentado** (no encendido) y se realizó la lectura. Para poder ubicar las lecturas temporalmente, se puede obtener la timestamp actual con el comando `getTimestamp`.


## Manipulación de parámetros de bajo nivel

```
Los comandos de bajo nivel del módulo lector RFID pueden manipularse directamente con los comandos getRRParam/setRRParam.
Los parámetros de interés son:

    /reader/gen2/Tari
    /reader/gen2/tagEncoding
    /reader/gen2/BLF

Y sus valores los dados por:

static const char *tariNames[] = {"TARI_25US", "TARI_12_5US", "TARI_6_25US"};
static const char *tagEncodingNames[] = {"FM0", "M2", "M4", "M8"};
static const char *gen2LinkFrequencyNames[] = {"LINK250KHZ", "LINK300KHZ", "LINK320KHZ", "LINK40KHZ", "LINK640KHZ"};

Ejemplo:

action=getRRParam&name=/reader/gen2/Tari
/reader/gen2/Tari=TARI_25US
action=getRRParam&name=/reader/gen2/tagEncoding
/reader/gen2/tagEncoding=M4
action=getRRParam&name=/reader/gen2/BLF
/reader/gen2/BLF=LINK250KHZ

# set
action=setRRParam&name=/reader/gen2/BLF&value=LINK640KHZ
/reader/gen2/BLF=LINK640KHZ

action=getRRParam&name=/reader/gen2/BLF
/reader/gen2/BLF=LINK640KHZ


Notas:

- Los settings por defecto del lector Thingmagic tras un reset son:

    action=getRRParam&name=/reader/gen2/TARI &value=TARI_6_25US
    /reader/gen2/TARI=TARI_25US
    action=getRRParam&name=/reader/gen2/tagEncoding &value=M4
    /reader/gen2/tagEncoding=M4
    action=getRRParam&name=/reader/gen2/BLF &value=M4
    /reader/gen2/BLF=LINK250KHZ

- El BLF "M8" parece estar soportado, pero al fijarlo, las lecturas dejan de funcionar, por lo que a todos los efectos no es operativo, aunque el manual de ThingMagic dice que sí está soportado

- TARI: Es el Tsimbolo lector -> tag. Cuanto más alto (TARI_6_25US -> TARI_12_5US -> TARI_25US), menor link rate, mejor penetración y menor interferencia entre lectores

- BLF: Velocidad cruda del canal tag -> lector

- tagEncoding: Número de repeticiones de cada símbolo tag -> lector


Para el control de la región tenemos lo siguiente:

    /reader/region/id           [read/write]
        Valores= {"OPEN", "NA", "EU", "KR", "IN", "JP", "PRC","EU2", "EU3", "KR2", "PRC2", "AU", "NZ", "NA2", "NA3", "IS", "MY", "ID", "PH", "TW", "MO", "RU", "SG", "JP2", "JP3", "VN", "TH", "AR", "HK", "BD","EU4"};
    /reader/region/hopTable     [readonly] , [readwrite (con valores razonables) si id="OPEN"]
      /reader/region/hopTime      [read/write]

Ejemplos:

action=getRRParam&name=/reader/region/id
/reader/region/id=EU3

action=getRRParam&name=/reader/region/hopTable
/reader/region/hopTable=[865700,866300,866900,867500]

action=getRRParam&name=/reader/region/hopTime
/reader/region/hopTime=3975

action=setRRParam&name=/reader/region/id&value=EU4
/reader/region/id=EU4

action=getRRParam&name=/reader/region/hopTable
/reader/region/hopTable=[916300,917500,918700]

action=getRRParam&name=/reader/region/hopTime
/reader/region/hopTime=3975

action=getRRParam&name=/reader/region/id&value=OPEN
/reader/region/id=OPEN

action=setRRParam&name=/reader/region/hopTable&value=[902000,903000]
/reader/region/hopTable=[902000,903000]

action=setRRParam&name=/reader/region/hopTime&value=100
/reader/region/hopTime=100


Para el control del "Q" (parámetro de control de acceso aleatorio al aire por la población de tags):

Es el parámetro

    /reader/gen2/q

que puede tomar los valores
    "DynamicQ" , ó
    "StaticQ(N)" , con N:[0,15]

Por defecto está en "DynamicQ"

Ejemplos:

action=setRRParam&name=/reader/gen2/q&value=DynamicQ
/reader/gen2/q=DynamicQ
action=setRRParam&name=/reader/gen2/q&value=StaticQ(0)
/reader/gen2/q=StaticQ(0)
action=setRRParam&name=/reader/gen2/q&value=StaticQ(8)
/reader/gen2/q=StaticQ(8)
action=getRRParam&name=/reader/gen2/q
/reader/gen2/q=StaticQ(8)
action=setRRParam&name=/reader/gen2/q&value=DynamicQ
/reader/gen2/q=DynamicQ
action=getRRParam&name=/reader/gen2/q
/reader/gen2/q=DynamicQ

```

## Multiplexación de antenas

*Control manual del multiplexor de Keonn:*

```
Notas:
- La serigrafía de los leds del multiplexor es errónea: el led blanco es ON, y los leds rojos son A y B, en ese orden, desde el led blanco.
- La tabla de verdad es 00->antena1, 01->antena2, 10->antena3, 11->antena4
- Los valores 0/1 de la tabla de verdad se corresponden con el valor eléctrico de la señal en la entrada del multiplexor, por lo que el valor está negado respecto al valor de activación de la salida del rflow, que es de colector abierto. Entonces tenemos:
  action=setIO&name=0&value=1 + action=setIO&name=1&value=1 -> antena1
  action=setIO&name=0&value=0 + action=setIO&name=1&value=1 -> antena2
  action=setIO&name=0&value=1 + action=setIO&name=1&value=0 -> antena3
  action=setIO&name=0&value=0 + action=setIO&name=1&value=0 -> antena4
```

*Control automático del multiplexor de Keonn, mediante configuración de mapa de antenas en el lector Thingmagic:*

```
Se configuran el número de salidas del lector a utilizar para el control de la multiplexación:

> action=setRRParam&name=/reader/antenna/portSwitchGpos&value=[1,2]

A partir de este momento, el lector reportará un número ampliado de puertos de antena:

> action=getRRParam&name=/reader/antenna/portList
< /reader/antenna/portList=[1,2,3,4,5,6,7,8]
> action=getRRParam&name=/reader/antenna/txRxMap
< /reader/antenna/txRxMap=[[1,1,1],[2,2,2],[3,3,3],[4,4,4],[5,5,5],[6,6,6],[7,7,7],[8,8,8]]

Para activar la lectura por las antenas:

> action=setRRParam&name=/reader/read/plan&value=SimpleReadPlan:[Antennas=[1,2,3,4,5,6,7,8],Protocol=GEN2,Filter=null,Op=null,UseFastSearch=false,Weight=1000]

La equivalencia/ordenamiento de antenas (el valor de GPIO es 0/inactivo, 1/activo, GPIO1|GPIO0) es la siguiente. El lector cicla primero cada uno de sus 2 puertos y luego los GPIO's. Entonces:

    Antena 1: puerto RF 1 + GPIO 00 => multiplexor 1 , antena 4
    Antena 2: puerto RF 2 + GPIO 00 => multiplexor 2 , antena 4
    Antena 3: puerto RF 1 + GPIO 01 => multiplexor 1 , antena 2
    Antena 4: puerto RF 2 + GPIO 01 => multiplexor 2 , antena 2
    Antena 5: puerto RF 1 + GPIO 10 => multiplexor 1 , antena 3
    Antena 6: puerto RF 2 + GPIO 10 => multiplexor 2 , antena 3
    Antena 7: puerto RF 1 + GPIO 11 => multiplexor 1 , antena 1
    Antena 8: puerto RF 2 + GPIO 11 => multiplexor 2 , antena 1

Puede configurarse la detección de antenas conectadas con el comando

> action=setRRParam&name=/reader/antenna/checkPort&value=true

Tras ello, el equipo reportará las antenas en la variable listada debajo. El mecanismo de detección se basa en la medida del Return Loss. Sin embargo, la antena interna del Rflow no parece detectable por este método y no aparece listada, al menos con la placa suelta del Rflow fuera de su caja.

> action=getRRParam&name=/reader/antenna/connectedPortList
< /reader/antenna/connectedPortList=[2,8]

Otros comandos relacionados o relevantes:

action=getRRParam&name=/reader/gpio/inputList
action=getRRParam&name=/reader/gpio/outputList
action=setRRParam&name=/reader/antenna/portSwitchGpos&value=[0]

action=getRRParam&name=/reader/antenna/settlingTimeList
action=getRRParam&name=/reader/radio/portReadPowerList
action=getRRParam&name=/reader/radio/portWritePowerList
```
