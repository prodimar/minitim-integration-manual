# Instrucciones de programación / actualización de firmware

Para escribir o actualizar un programa binario (denominado `target.bin` en el contexto de estas instrucciones) en la memoria de un dispositivo Minitim, es necesario descargar la herramienta **Flash Download Tool**, disponible en [esta ubicación](https://www.espressif.com/en/support/download/other-tools). Se trata de una herramienta portable que no necesita instalación.

Al iniciar el ejecutable, se muestra la siguiente pantalla.

![Flash Download Tools pantalla 1](media_files/flash_download_tools_p1.png "Pantalla de selección de chip")

Se debe seleccionar el botón etiquetado como *ESP32 DownloadTool*. Se abrirá una nueva ventana con el siguiente aspecto.

![Flash Download Tools pantalla 2](media_files/flash_download_tools_p2.png "Pantalla de configuración")

En ella se pueden configurar diversos parámetros para la descarga del programa.

En uno de los cuadros de texto de la parte superior se debe proporcionar la ruta del binario `target.bin`, y en el cuadro de texto de su derecha la dirección de memoria `0x0000` donde el programa será escrito. Se debe marcar la casilla situada a la izquierda de esa línea. Si cualquiera de estos datos no es correcto o no se ha especificado, el fondo del campo de texto se coloreará de rojo.

Las opciones de la sección etiquetada como *SpiFlashConfig* deben configurarse como en la captura de pantalla superior.

En la sección etiquetada como *Download Panel 1*, hay que seleccionar el puerto COM a través del que está conectado el dispositivo (información disponible en el Administrador de dispositivos en los sistemas Windows) y la tasa de baudios de la transmisión (se puede seleccionar cualquier valor de los disponibles en el desplegable). Para comenzar y parar la descarga, se usan los botones a la izquierda de éstos selectores.

El estado de la transmisión se indica en el cuadro de color de la misma sección; muestra `IDLE` cuando no se está ejecutando ninguna transmisión, `Download` cuando está en proceso y `FINISH` cuando se ha terminado con éxito. En los casos de error, indica la causa del error y cambia su color.

Tras completar la programación con éxito, ***es necesario reiniciar el dispositivo***. Para que el reinicio tenga éxito, es necesario seguir esta secuencia de pasos:

 - Desconectar la alimentación del dispositivo (retirar el cable usado para la descarga del binario).
 - Retirar la batería.
 - Volver a insertar la batería.

Sin este proceso, el dispositivo no reconocerá el nuevo programa en memoria.

## Programación de dispositivos con particionado OTA

Para dispositivos cuyo firmware implementa OTA con múltiples particiones un layout típico de las mismas sería el de la captura de pantalla de debajo (el ejemplo se corresponde con el firmware Rflow v2.1):

![Flash Download Tools multipartición](media_files/flash_download_tools_ota_partition_layout.png "Layout de particiones OTA")

## Generación de 1 archivo único para programación en fábrica

Para la generación de un archivo único de programación, basta preparar el layout descrito en la sección anterior, consistente en:

  - @0x1000: build\bootloader\bootloader.bin
  - @0x8000: build\partition_table\partition-table.bin
  - @0xd000: build\partition_table\partition-table.bin
  - @0x10000: build\firmware.bin (nombre del proyecto en vez de "firmware")

Y pulsar el botón **CombineBin**. Esto generará un archivo binario target.bin, consistente en la concatenación de las 4 zonas listadas y un padding inicial de 4kbytes (0x1000) a 0xFF.

Este es el archivo a propagar para fabricación, utilizando el sufijo `_combined` (ejemplo: `rflow1-v4.8-hwversion8_combined.bin`).

Para la actualización OTA no se utiliza este archivo, sino directamente el obtenido como build\firmware.bin.